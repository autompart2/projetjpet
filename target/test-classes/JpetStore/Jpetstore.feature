Feature: Connexion � Jpetstore
  
  Scenario Outline: Connexion
    Given Un navigateur est ouvert
    When Je suis sur la page d accueil
    And Je clique sur le lien de connexion
    And Je rentre le Username <Username> 
    And Je rentre le Password <Password> 
    Then Je clique sur Login
    Then Utilisateur ABC est connecte
    And Je peux lire le message accueil <returnMessage> 
    
    Examples:
    |			Username	|		Password		|			returnMessage			|
    |			"j2ee"		|			"j2ee"		|			"Welcome ABC!"		|
    |			"j2ee"		|		  "j2ee"		|			"Welcome ABC!"		|