package floorball;

import io.cucumber.java.en.*;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import  org.openqa.selenium.chrome.ChromeDriver;

public class floorballSteps {
	
	
	WebDriver driver;


	@Given("I have opened a browser")
	public void i_have_opened_a_browser() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "./rsc/chromedriver.exe");
		driver = new ChromeDriver();
	    driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	    driver.get("https://www.google.fr");
	    driver.findElement(By.xpath("//div[1][contains(text(),'Tout accepter')]")).click();
	}

	@Given("I search for floorball France")
	public void i_search_for_floorball_France() throws Throwable {
	    driver.findElement(By.xpath("//textarea[@class='gLFyf']")).sendKeys("France Floorball");
	    driver.findElement(By.xpath("//textarea[@class='gLFyf']")).sendKeys(Keys.ENTER);
	}

	@When("I click on the French floorball federation website")
	public void i_click_on_the_french_floorball_federation_website() throws Throwable {
	    driver.findElement(By.xpath("//h3[@class=\"LC20lb MBeuO DKV0Md\"]")).click();
	}

	@Then("the menu ou pratiquer is clickable")
	public void the_menu_is_clickable() throws Throwable {
	    Thread.sleep(300);
		driver.findElement(By.xpath("//a[@href=\"spip.php?page=maps02\"")).click();
	}
}
