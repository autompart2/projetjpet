package jpetstoreSteps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class jpetStore {
	
	WebDriver driver;

	
	@Given("Un navigateur est ouvert")
	public void un_navigateur_est_ouvert() {
		System.setProperty("webdriver.chrome.driver", "./rsc/chromedriver.exe");
	    driver = new ChromeDriver();
	    driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	    driver.get("https://petstore.octoperf.com/actions/Catalog.action");
	}

	@When("Je suis sur la page d accueil")
	public void je_suis_sur_la_page_d_accueil() {
		String homepage= "JPetStore";

	    if (driver.getTitle().equals(homepage))
	    {
	        System.out.println("Nous sommes sur la bonne page");
	    }
	    else
	    {
	        System.out.println("Nous ne sommes pas sur la bonne page");
	    }
	}

	@When("Je clique sur le lien de connexion")
	public void je_clique_sur_le_lien_de_connexion() {
		driver.findElement(By.xpath("//a[.='Sign In']")).click();
	}

	@When("Je rentre le Username {string}")
	public void je_rentre_le_Username(String login) {
		//driver.findElement(By.xpath("//input[@name='username']")).click();
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys(login);
	}

	@When("Je rentre le Password {string}")
	public void je_rentre_le_Password(String password) {
		driver.findElement(By.xpath("//input[@name='password']")).clear();
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys(password);
	}

	@Then("Je clique sur Login")
	public void je_clique_sur_Login() {
		driver.findElement(By.xpath("//input[@value='Login']")).click();
	}

	@Then("Utilisateur ABC est connecte")
	public void utilisateur_ABC_est_connect() {
		boolean verifConnexion = driver.findElement(By.xpath("//a[.='Sign Out']")).isDisplayed();
		assertTrue(verifConnexion);
		System.out.println("Nous ne sommes connect�");
	}

	@Then("Je peux lire le message accueil {string}")
	public void je_peux_lire_le_message_accueil(String messageAccueil) {
		String verifMessage = driver.findElement(By.xpath("//div[contains(text(),'Welcome ABC!')]")).getText();
		assertEquals(messageAccueil,verifMessage);
		System.out.println("Le message d'accueil est pr�sent et correct");
	}


}
